#!/bin/sh

if ! [[ -z "${REPO_URL}" ]]; then
  sh ${GIT_HELPERS_DIR}/setupRepoAccess.sh
fi

php-fpm83
nginx -g "daemon off;"
