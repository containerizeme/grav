# User
ARG USER=www
ARG GROUP=www
ARG UID=1000
ARG GID=1000
ARG REPO_DIR=/opt/repo

ARG BASE_IMG_VERSION=3.19.0-r1-onbuild
ARG DOWNLOAD_DIR=/tmp/download

# Grav downloader
FROM alpine:latest as downloader

# Common arguments must be repeated but kept empty to be accepted
ARG DOWNLOAD_DIR

ARG GRAV_VERSION=1.7.43
ARG TMP_DOWNLOAD_DIR=/tmp/grav

RUN mkdir -p ${TMP_DOWNLOAD_DIR} && \
    mkdir ${DOWNLOAD_DIR}

RUN wget https://github.com/getgrav/grav/releases/download/${GRAV_VERSION}/grav-v${GRAV_VERSION}.zip -P ${TMP_DOWNLOAD_DIR}
RUN unzip ${TMP_DOWNLOAD_DIR}/grav-v${GRAV_VERSION}.zip -d ${DOWNLOAD_DIR}

FROM icebear8/gitrepoutils:${BASE_IMG_VERSION}
# Common arguments must be repeated but kept empty to be accepted
ARG DOWNLOAD_DIR

ARG BASE_DIR=/opt
ARG MAIN_APP_DIR=${BASE_DIR}/grav

ENV REPO_URL=
ENV REPO_DIR=${REPO_DIR}
ENV WEB_CONTENT_DIR=${REPO_DIR}/web

ENV GIT_UTILS_DIR=${BASE_DIR}/utils/git
ENV GRAV_UTILS_DIR=${BASE_DIR}/utils/grav
ENV APP_LAUNCHER=${GRAV_UTILS_DIR}/launcher.sh

RUN apk update \
  && apk add --no-cache \
  logrotate \
  nginx \
  php83 \
  php83-ctype \
  php83-curl \
  php83-dom \
  php83-fpm \
  php83-gd \
  php83-intl \
  php83-mbstring \
  php83-opcache \
  php83-openssl \
  php83-pecl-apcu \
  php83-pecl-yaml \
  php83-phar \
  php83-redis \
  php83-session \
  php83-simplexml \
  php83-tokenizer \
  php83-xml \
  php83-zip \
  && apk del --purge

# User is created by base image

# Prepare nginx
COPY /resources/nginx/nginx.conf /etc/nginx/nginx.conf
COPY /resources/nginx/config/* /etc/nginx/sites-enabled/
RUN touch /var/run/nginx.pid

# Prepare utilities
RUN mkdir -p ${GRAV_UTILS_DIR}
COPY /utils/* ${GRAV_UTILS_DIR}/

# Create application directory and copy grav
RUN mkdir -p ${BASE_DIR}
COPY --from=downloader ${DOWNLOAD_DIR}/ ${BASE_DIR}

# Move the web content into a separate directory and create a symlink
RUN mkdir -p ${REPO_DIR} \
    && mkdir -p ${WEB_CONTENT_DIR} \
    && mv ${MAIN_APP_DIR}/user ${WEB_CONTENT_DIR}/user \
    && ln -s ${WEB_CONTENT_DIR}/user ${MAIN_APP_DIR}/user

# Setup update script, it can be used if it is located in the nginx base directory
COPY /resources/deploy.php ${MAIN_APP_DIR}/

# Give access to the resources for the specified user
RUN chown -R ${USER}:${GROUP} /home/${USER} && \
    chown -R ${USER}:${GROUP} ${MAIN_APP_DIR} && \
    chmod -R 755 ${GRAV_UTILS_DIR}/*.sh && \
    chown -R ${USER}:${GROUP} ${REPO_DIR} && \
    chown -R ${USER}:${GROUP} /var/log && \
    chown -R ${USER}:${GROUP} /var/lib/nginx && \
    chown -R ${USER}:${GROUP} /var/run/nginx.pid

USER ${USER}
WORKDIR ${REPO_DIR}
VOLUME ${REPO_DIR}
EXPOSE 8080

ENTRYPOINT ["sh", "-c"]
CMD ["sh ${APP_LAUNCHER}"]
