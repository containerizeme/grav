[![Pipeline Master](https://img.shields.io/gitlab/pipeline/containerizeme/grav/master?label=master&logo=gitlab)](https://gitlab.com/containerizeme/grav)
[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17671273%2Fissues_statistics)](https://gitlab.com/containerizeme/grav/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17671273%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/containerizeme/grav/-/commits/master)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17671273%3Flicense%3Dtrue)](https://gitlab.com/containerizeme/grav/-/blob/master/LICENSE)
applies to software of this project. The running software within the image/container ships with its own license(s).

[![Project](https://badgen.net/badge/project/Grav/orange?icon=gitlab)](https://gitlab.com/containerizeme/grav/-/blob/master/README.md#grav)
[![Stable Version](https://img.shields.io/docker/v/icebear8/grav/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/grav/-/blob/master/CHANGELOG.md#grav)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/grav?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/grav)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/grav/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/grav)

# Grav
<img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/17671273/grav.png" alt="Avatar" height="50" />
[Grav](https://getgrav.org/) is a modern open source flat-file CMS and allows to manage website content with markdown files.

This image contains everything to run Grav within a docker container.
Nginx is set up to publish the Grav website.
Additionally a link to a (private or public) git repository can be used to manage the maintain the content.

# Usage
`docker run -p 8080:8080 icebear8/grav`

# Setup Content on Github
The container supports managing the content on a Github repository.
See [GitRepoUtils](https://gitlab.com/containerizeme/utils/-/blob/master/README.md) for detailed usage instructions.

##  Folder Structure for Website Content
* Create the folder `web/user` in the repository with the website content
* The `user` folder corresponds with the `user` folder as documented by [Grav](https://getgrav.org/) and should contain the following subfolders
  * `accounts`, `config`, `data`, `pages`, `plugins`, `themes`
  * The content in markdown format of the website is stored in the `pages` folder

##  Update at Runtime
* A php deploy script (`http://<yourUrl>/deploy.php`) is used to update the website content
* Calling the script will pull the latest changes from the remote repository
